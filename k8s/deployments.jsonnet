local deployment = import "../../releng/hugo-websites/kube-deployment.jsonnet";

deployment.newProductionDeploymentWithStaging(
  "sdv.eclipse.org", "sdv-staging.eclipse.org"
)